JAVA_HOME=	/usr/local/openjdk8

all: clean build jar run

Branding:
	(cd src && ${JAVA_HOME}/bin/javac Branding.java)

Main:
	(cd src && ${JAVA_HOME}/bin/javac Main.java)

Elements:
	(cd src && ${JAVA_HOME}/bin/javac Elements.java)

OpenFileIF:
	(cd src && ${JAVA_HOME}/bin/javac OpenFileIF.java)

OpenFileOF:
	(cd src && ${JAVA_HOME}/bin/javac OpenFileOF.java)

WriteTask:
	(cd src && ${JAVA_HOME}/bin/javac WriteTask.java)

build: clean Elements OpenFileIF OpenFileOF WriteTask Branding Main

run: build
	(cd src && ${JAVA_HOME}/bin/java Main)

clean:
	(cd src && rm -rvf *.class)

jar: build
	(cd src && ${JAVA_HOME}/bin/jar cvfe ../dist/dd-gui-0.1.jar Main *.class)
