# dd-gui-java

A java rewrite of dd-gui

It's not yet finished, uploaded just to not do it later.

### Building

make sure that you have java 8 or greater installed

clone this repository, go to in and type:

	mkdir -p dist
	make jar

Then to launch, type:

	java -jar dist/dd-gui*.jar

You may also use the gui desktop file if you want.
