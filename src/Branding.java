public class Branding
{
    public static final String app_name = "dd-gui";
    public static final double app_version = 0.1;
    public static final String manufacturer = "glowiak";
    
    public static final int WIDTH = 500;
    public static final int HEIGHT = 200;
    
    public static final int BUTT_WIDTH = 100;
    public static final int BUTT_HEIGHT = 20;
    
    public static final String DD_PATH = "dd";
    public static final String XZCAT_PATH = "xzcat";
    public static final String DOWNLOAD_PATH = String.format("%s/.dd-gui/downloads", System.getenv("HOME"));
}
