import java.io.*;
import java.nio.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.net.URL;
import java.nio.channels.*;
import java.lang.Math;
import java.nio.file.*;
import java.util.*;

public class WriteTask implements ActionListener, Elements
{
    public void actionPerformed(ActionEvent e)
    {
        try {
            doStuff();
        } catch(IOException zeta) {
            System.out.println(zeta);
        }
    }
    public void doStuff() throws IOException
    {
        Branding brand = new Branding();
        Main mn = new Main();
        File file_in = new File(INPUT_IF.getText());
        if (!file_in.exists() && !INPUT_IF.getText().contains("http://") && !INPUT_IF.getText().contains("https://"))
        {
            JOptionPane.showMessageDialog(w, "File does not exist and it's not an URL");
            System.exit(1);
        }
        String WRITE_BS = "";
        String WRITE_COUNT = "";
        String WRITE_SKIP = "";
        String WRITE_CONV = "";
        if (CB_BS.isSelected())
        {
            WRITE_BS = String.format("bs=%s", INPUT_BS.getText());
        } else {
            WRITE_BS = "";
        }
        if (CB_COUNT.isSelected())
        {
            WRITE_COUNT = String.format("count=%s", INPUT_COUNT.getText());
        } else {
            WRITE_COUNT = "";
        }
        if (CB_SKIP.isSelected())
        {
            WRITE_SKIP = String.format("skip=%s", INPUT_SKIP.getText());
        } else {
            WRITE_SKIP = "";
        }
        if (CB_CONV.isSelected())
        {
            WRITE_CONV = String.format("conv=%s", DRP_CONV.getSelectedItem().toString());
        } else {
            WRITE_CONV = "";
        }
        
        // http moment
        if (INPUT_IF.getText().contains("http://") || INPUT_IF.getText().contains("https://"))
        {
            System.out.println(String.format("Downloading file %s", INPUT_IF.getText()));
            
            mn.setStatus("downloading...");
            
            // popular image extensions with their xzipped and lzmaed versions
            String nextExt = "";
            if (INPUT_IF.getText().contains(".xz"))
            {
                nextExt = "iso.xz";
            }
            if (INPUT_IF.getText().contains(".lzma"))
            {
                nextExt = "iso.lzma";
            }
            if (INPUT_IF.getText().contains(".iso") || INPUT_IF.getText().contains(".ISO")) // Holy fix for TempleOS
            {
                nextExt = "iso";
            }
            if (INPUT_IF.getText().contains(".img"))
            {
                nextExt = "img";
            }
            if (INPUT_IF.getText().contains(".raw")) // used by OpenBSD
            {
                nextExt = "raw";
            }
            
            URL getIso = new URL(INPUT_IF.getText());
            String newF_path = String.format("%s/%s.%s", brand.DOWNLOAD_PATH, Math.random(), nextExt);
            Path download_path = Paths.get(newF_path);
            InputStream isHere = getIso.openStream();
            Files.copy(isHere, download_path, StandardCopyOption.REPLACE_EXISTING);
            INPUT_IF.setText(newF_path);
            
            mn.setStatus("idle"); // reset the status
        }
        
        
        // String EXEC_CMD;
        
        //if (INPUT_IF.getText().contains(".img.xz") || INPUT_IF.getText().contains(".iso.xz") || INPUT_IF.getText().contains(".raw.xz") || INPUT_IF.getText().contains(".img.lzma") || INPUT_IF.getText().contains(".iso.lzma") || INPUT_IF.getText().contains(".raw.lzma"))
        //{
        //    String EXEC_CMD = String.format("%s '%s' | %s of='%s' %s %s %s %s status=progress", brand.XZCAT_PATH, INPUT_IF.getText(), brand.DD_PATH, INPUT_OF.getText(), WRITE_BS, WRITE_COUNT, WRITE_SKIP, WRITE_CONV);
        //} else {
        String EXEC_CMD = String.format("%s if='%s' of='%s' %s %s %s %s status=progress", brand.DD_PATH, INPUT_IF.getText(), INPUT_OF.getText(), WRITE_BS, WRITE_COUNT, WRITE_SKIP, WRITE_CONV);
        // }
        EXEC_CMD = String.format("echo 'hello=world, %s' > /tmp/helo", System.getenv("HOME"));
        
        System.out.println(String.format("Command to be executed: %s", EXEC_CMD));
        
        // device checks and warnings
        if (INPUT_OF.getText().contains("/dev/"))
        {
            File ifEx = new File(INPUT_OF.getText());
            if (!ifEx.exists())
            {
                JOptionPane.showMessageDialog(w, "Device not found");
                System.exit(1);
            }
            
            int agreed = JOptionPane.showConfirmDialog(w, String.format("You want to write to %s device! Make sure that it's correct and that you have root access!", INPUT_OF.getText()));
            
            if (agreed != JOptionPane.YES_OPTION)
            {
                System.exit(0);
            }
        }
        
        mn.setStatus("writing...");
        System.out.println("WRITE BEGIN");
        
        
        Process process = Runtime.getRuntime().exec(EXEC_CMD);
        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line = "";
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }
        
        System.out.println("finished");
        mn.setStatus("idle");
    }
}
