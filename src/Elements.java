import java.io.*;
import javax.swing.*;
import java.nio.*;
import java.awt.*;
import java.awt.event.*;

public interface Elements // here goes all the *new* stuff
{
    JLabel TEXT_STATUS = new JLabel();
    JLabel TEXT_IF = new JLabel("Input file:");
    JLabel TEXT_OF = new JLabel("Output file:");
    JButton BUTT_IF = new JButton("Browse");
    JButton BUTT_OF = new JButton("Browse");
    JTextField INPUT_IF = new JTextField("/path/to/image.iso");
    JTextField INPUT_OF = new JTextField("/dev/sdXX");
    JCheckBox CB_BS = new JCheckBox("Block size:");
    JTextField INPUT_BS = new JTextField("1m");
    JCheckBox CB_COUNT = new JCheckBox("Count:");
    JTextField INPUT_COUNT = new JTextField("512");
    JCheckBox CB_SKIP = new JCheckBox("Skip:");
    JTextField INPUT_SKIP = new JTextField("12");
    JCheckBox CB_CONV = new JCheckBox("Convert:");
    String conversions[] = { "ascii", "block", "ibm", "fdatasync", "fsync", "lcase", "pareven", "noerror", "notrunc", "osync", "sparse", "swab", "sync", "ucase", "unblock" };
    JComboBox DRP_CONV = new JComboBox(conversions);
    JButton BUTT_WRITE = new JButton("Write");
    JFrame w = new JFrame();
}
