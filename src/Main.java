import java.io.*;
import javax.swing.*;
import java.nio.*;
import java.awt.*;
import java.awt.event.*;

public class Main implements Elements // here goes all other stuff
{
    public static void main(String[] args)
    {
        Branding brand = new Branding();
        OpenFileIF open_if = new OpenFileIF();
        OpenFileOF open_of = new OpenFileOF();
        WriteTask wt = new WriteTask();
        w.setTitle(String.format("%s v%.1f", brand.app_name, brand.app_version));
        w.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        w.setSize(brand.WIDTH,brand.HEIGHT);
        
        File mkD = new File(brand.DOWNLOAD_PATH);
        if (!mkD.exists())
        {
            System.out.println(String.format("Creating directory %s", brand.DOWNLOAD_PATH));
            mkD.mkdirs();
        }
        
        System.out.println(String.format("INFO: dd path is %s", brand.DD_PATH));
        TEXT_IF.setBounds(1, 23, 100, 30);
        TEXT_OF.setBounds(1, 48, 100, 30);
        BUTT_IF.setBounds(brand.WIDTH - 114, 28, brand.BUTT_WIDTH, brand.BUTT_HEIGHT);
        BUTT_OF.setBounds(brand.WIDTH - 114, 53, brand.BUTT_WIDTH, brand.BUTT_HEIGHT);
        INPUT_IF.setBounds(90, 28, 295, 20);
        INPUT_OF.setBounds(90, 53, 295, 20);
        CB_BS.setBounds(0,70,100,30);
        INPUT_BS.setBounds(100, 75, 50, 20);
        CB_COUNT.setBounds(150, 73, 72, 25);
        INPUT_COUNT.setBounds(225, 75, 50, 20);
        CB_SKIP.setBounds(275, 73, 65, 25);
        INPUT_SKIP.setBounds(340, 75, 72, 20);
        setStatus("idle");
        TEXT_STATUS.setBounds(1,145,100,30);
        CB_CONV.setBounds(1,95, 85, 25);
        DRP_CONV.setBounds(86, 100, 85, 20);
        BUTT_WRITE.setBounds(387, 150, brand.BUTT_WIDTH, brand.BUTT_HEIGHT);
        BUTT_IF.addActionListener(open_if);
        BUTT_OF.addActionListener(open_of);
        BUTT_WRITE.addActionListener(wt);
        
        w.add(BUTT_IF);
        w.add(BUTT_OF);
        w.add(TEXT_IF);
        w.add(TEXT_OF);
        w.add(INPUT_IF);
        w.add(INPUT_OF);
        w.add(CB_BS);
        w.add(INPUT_BS);
        w.add(CB_COUNT);
        w.add(INPUT_COUNT);
        w.add(CB_SKIP);
        w.add(INPUT_SKIP);
        w.add(TEXT_STATUS);
        w.add(CB_CONV);
        w.add(DRP_CONV);
        w.add(BUTT_WRITE);
        
        w.setLocationRelativeTo(null);
        w.setLayout(null);
        w.setVisible(true);
    }
    public static void setStatus(String status2)
    {
        TEXT_STATUS.setText(String.format("Status: %s", status2));
    }
}
